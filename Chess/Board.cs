﻿using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Text;
using System.Threading;

namespace Chess
{
    /// <summary>
    /// Board class
    /// </summary>
    public class Board
    {
        /// <summary>
        /// Fen string
        /// </summary>
        public string fen { get; private set; }

        /// <summary>
        /// Array figures
        /// </summary>
        private EFigure[,] figures;

        /// <summary>
        /// Move color
        /// </summary>
        public EColor moveColor { get; private set; }

        /// <summary>
        /// Move number
        /// </summary>
        public int moveNumber { get; private set; }
        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="fen"></param>
        public Board(string fen)
        {
            this.fen = fen;
            figures = new EFigure[8, 8];
            Init();
        }

        /// <summary>
        /// Init fields
        /// </summary>
        private void Init()
        {
            //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
            // 0                                           1 2    3 4 5
            // 0 - расположение фигур
            // 1 - кто ходит? белые или чёрные
            // 2 - флаги рокировки
            // 3 - битое поле
            // 4 - количество ходов для "правила 50 ходов"
            // 5 - номер хода сейчас

            string[] parts = fen.Split();
            if (parts.Length != 6)
            {
                return;
            }
            InitFigures(parts[0]);
            moveColor = parts[1] == "b" ? EColor.Black : EColor.White;
            moveNumber = int.Parse(parts[5]);
            moveColor = EColor.White;
        }

        void InitFigures(string data)
        {
            for (int j = 8; j >= 2; j--)
            {
                data = data.Replace(j.ToString(), (j - 1).ToString() + "1");
            }
            data = data.Replace("1", ".");
            string[] lines = data.Split('/');
            for (int y = 7; y >= 0; y--)
            {
                for (int x = 0; x < 8; x++)
                {
                    figures[x, y] = lines[7 - y][x] == '.' ? EFigure.None :
                          (EFigure) lines[7 - y][x];
                }
            }
        }

        private void GenerateFen()
        {
            fen = FenFigures() + " " +
                   (moveColor == EColor.White ? "w" : "b") + 
                   " - - 0 " + moveNumber.ToString();
        }

        private string FenFigures()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 7; y >= 0; y--)
            {
                for (int x = 0; x < 8; x++)
                {
                    sb.Append(figures[x, y] == EFigure.None ? '1' : (char)figures[x, y]);
                }

                if (y > 0)
                {
                    sb.Append('/');
                }
            }

            string eigth = new string('1', 8);
            for (int j = 8; j >= 2; j--)
            {
                sb.Replace(eigth.Substring(0, j), j.ToString());
            }
            return sb.ToString();
        }
        /// <summary>
        /// Get figure at square
        /// </summary>
        /// <param name="square">Square</param>
        /// <returns>Gigure on board or None</returns>
        public EFigure GetFigureAt(Square square)
        {
            if (square.OnBoard())
            {
                return figures[square.x, square.y];
            }
            return EFigure.None;
        }

        /// <summary>
        /// Set figure
        /// </summary>
        /// <param name="square">Square</param>
        /// <param name="figure">Figure</param>
        private void SetFigureAt(Square square, EFigure figure)
        {
            if (square.OnBoard())
            {
                figures[square.x, square.y] = figure;
            }
        }

        /// <summary>
        /// Make move
        /// </summary>
        /// <param name="fm">Figure moving</param>
        /// <returns>Next move</returns>
        public Board Move (FigureMoving fm)
        {
            Board next = new Board(fen);
            next.SetFigureAt(fm.from, EFigure.None);
            next.SetFigureAt(fm.to, fm.promotion == EFigure.None ? fm.figure : fm.promotion);
            if (moveColor == EColor.Black)
            {
                next.moveNumber++;
            }

            next.moveColor = moveColor.FlipColor();
            next.GenerateFen();
            return next;
        }
    }
}