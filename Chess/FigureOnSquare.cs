﻿namespace Chess
{
    /// <summary>
    /// Figure on square postion 
    /// </summary>
    public class FigureOnSquare
    {
        /// <summary>
        /// Figure
        /// </summary>
        public EFigure figure { get; private set; }

        /// <summary>
        /// Square
        /// </summary>
        public Square square { get; private set; }

        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="figure">Figure</param>
        /// <param name="square">Square</param>
        public FigureOnSquare(EFigure figure, Square square)
        {
            this.figure = figure;
            this.square = square;
        }
    }
}