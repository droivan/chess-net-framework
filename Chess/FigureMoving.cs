﻿using System;
using System.Data.SqlClient;

namespace Chess
{
    public class FigureMoving
    {
        /// <summary>
        /// Figure
        /// </summary>
        public EFigure figure { get; private set; }

        /// <summary>
        /// Source position
        /// </summary>
        public Square from  { get; private set; }

        /// <summary>
        /// Destination position
        /// </summary>
        public Square to  { get; private set; }

        /// <summary>
        /// Figure promotion
        /// </summary>
        public EFigure promotion  { get; private set; }

        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="fs">Figure on square postion </param>
        /// <param name="to">Destination position</param>
        /// <param name="promotion">Figure promotion</param>
        public FigureMoving(FigureOnSquare fs, Square to, EFigure promotion = EFigure.None)
        {
            this.figure = fs.figure;
            this.from = fs.square;
            this.to = to;
            this.promotion = promotion;
        }

        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="move">Position square</param>
        public FigureMoving(string move) // Pe2e4   Pe7e8Q
        {                                // 01234   012345
            this.figure = (EFigure) move[0];
            this.from = new Square(move.Substring(1, 2));
            this.to = new Square(move.Substring(3,2));
            this.promotion = (move.Length == 6) ? (EFigure) move[5] : EFigure.None;
        }

        /// <summary>
        /// Delta x
        /// </summary>
        public int DeltaX { get { return to.x - from.x;  } }

        /// <summary>
        /// Delta y
        /// </summary>
        public int DeltaY { get { return to.y - from.y; } }

        /// <summary>
        /// Abs delta x
        /// </summary>
        public int AbsDeltaX { get { return Math.Abs(DeltaX); } }

        /// <summary>
        /// Abs delta y
        /// </summary>
        public int AbsDeltaY { get { return Math.Abs(DeltaY); } }

        /// <summary>
        /// Abs sign delta  x
        /// </summary>
        public int AbsSignX { get { return Math.Sign(DeltaX); } }

        /// <summary>
        /// Abs sign delta y
        /// </summary>
        public int AbsSignY { get { return Math.Sign(DeltaY); } }

    }
}