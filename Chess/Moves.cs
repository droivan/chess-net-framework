﻿using System;

namespace Chess
{
    public class Moves
    {
        private FigureMoving fm;
        private Board board;

        public Moves(Board board)
        {
            this.board = board;
        }

        public bool CanMove(FigureMoving fm)
        {
            this.fm = fm;
            return
                CanMoveFrom() &&
                CanMoveTo() &&
                CanFigureMove();
        }

        private bool CanMoveFrom()
        {
            return fm.from.OnBoard() &&
                   fm.figure.GetColor() == board.moveColor;
        }

        private bool CanMoveTo()
        {
            return fm.to.OnBoard() &&
                   fm.from != fm.to &&
                   board.GetFigureAt(fm.to).GetColor() != board.moveColor;
        }

        private bool CanFigureMove()
        {
            switch (fm.figure)
            {
                case EFigure.WhiteKing:
                case EFigure.BlackKing:
                    return CanKingMove();

                case EFigure.WhiteQueen:
                case EFigure.BlackQueen:
                    return CanStraigthMove();

                case EFigure.WhiteRook:
                case EFigure.BlackRook:
                    return false;


                case EFigure.WhiteBishop:
                case EFigure.BlackBishop:
                    return false;

                case EFigure.WhiteKnite:
                case EFigure.BlackKnite:
                    return CanKniteMove();

                case EFigure.WhitePawn:
                case EFigure.BlackPawn:
                    return false;

                default:
                    return false;
            }
        }

        private bool CanStraigthMove()
        {
            
        }

        private bool CanKingMove()
        {
            // TODO: написать рокировку
            if (fm.AbsDeltaX <= 1 && fm.AbsDeltaY <= 1)
                return true;
            return false;

        }

        private bool CanKniteMove()
        {
            if (fm.AbsDeltaX == 1 && fm.AbsDeltaY == 2)
            {
                return true;
            }
            if (fm.AbsDeltaX == 2 && fm.AbsDeltaY == 1)
            {
                return true;
            }
            return false;
        }

    }
}