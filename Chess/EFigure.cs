﻿using System.Runtime.Remoting;

namespace Chess
{
    /// <summary>
    /// Фигуры
    /// </summary>
    public enum EFigure
    {
        /// <summary>
        /// Нет фигуры
        /// </summary>
        None,

        /// <summary>
        /// Белый король
        /// </summary>
        WhiteKing = 'K',

        /// <summary>
        /// Белая королева
        /// </summary>
        WhiteQueen = 'Q',

        /// <summary>
        /// Белая ладья
        /// </summary>
        WhiteRook = 'R',

        /// <summary>
        /// Белый слон
        /// </summary>
        WhiteBishop = 'B',

        /// <summary>
        /// Берый конь
        /// </summary>
        WhiteKnite = 'N',

        /// <summary>
        /// Белая пешка
        /// </summary>
        WhitePawn = 'P',

        /// <summary>
        /// Чёрный король
        /// </summary>
        BlackKing = 'k',

        /// <summary>
        /// Чёрная королева
        /// </summary>
        BlackQueen = 'q',

        /// <summary>
        /// Чёрная ладья
        /// </summary>
        BlackRook = 'r',

        /// <summary>
        /// Чёрный слон
        /// </summary>
        BlackBishop = 'b',

        /// <summary>
        /// Чёрный конь
        /// </summary>
        BlackKnite = 'n',

        /// <summary>
        /// Чёрная пешка
        /// </summary>
        BlackPawn = 'p',
    }

    static class FigureMethods
    {
        public static EColor GetColor(this EFigure figure)
        {
            if (figure == EFigure.None)
                return EColor.None;
            return (figure == EFigure.WhiteKing ||
                    figure == EFigure.WhiteQueen ||
                    figure == EFigure.WhiteRook ||
                    figure == EFigure.WhiteBishop ||
                    figure == EFigure.WhiteKnite ||
                    figure == EFigure.WhitePawn)
                ? EColor.White
                : EColor.Black;
        }
    }
}