﻿using System.Runtime.CompilerServices;

namespace Chess
{
    /// <summary>
    /// Struct square
    /// </summary>
    public struct Square
    {
        /// <summary>
        /// Default error position square
        /// </summary>
        private static Square none = new Square(-1, -1);
        /// <summary>
        /// Low position
        /// </summary>
        private const int LowPosition = 0;

        /// <summary>
        /// High position
        /// </summary>
        private const int HighPosition = 8;

        /// <summary>
        /// Point x
        /// </summary>
        public int x { get; private set; }

        /// <summary>
        /// Point y
        /// </summary>
        public int y { get; private set; }

        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="e2">Position square</param>
        public Square (string e2)
        {
            if (e2.Length == 2 &&
                e2[0] >= 'a' && e2[0] <= 'h' &&
                e2[1] >= '1' && e2[1] <= '8')
            {
                x = e2[0] - 'a';
                y = e2[1] - '1';
            }
            else
            {
                this = none;
            }
        }

        /// <summary>
        /// c-tor
        /// </summary>
        /// <param name="x">Point x</param>
        /// <param name="y">Point y</param>
        public Square(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Check figure position
        /// </summary>
        /// <returns>CheckResult</returns>
        public bool OnBoard()
        {
            return x >= LowPosition && x <= HighPosition && 
                   y >= LowPosition && y <= HighPosition;
        }

        public static bool operator == (Square a, Square b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(Square a, Square b)
        {
            return a.x != b.x || a.y != b.y;
        }
    }
}