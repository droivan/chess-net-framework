﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Chess
    {
        // TODO: Написать алгоритм взятие на проходе
        // TODO: Написать алгоритм рокеровка
        public string fen { get; private set; }
        private Board board;
        private Moves moves;
        public Chess(string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        {
            this.fen = fen;
            board = new Board(fen);
            moves = new Moves(board);
        }

        Chess(Board board)
        {
            this.board = board;
            this.fen = board.fen;
            moves = new Moves(board);
        }
        public Chess Move(string move) // Pe2e4 Pe7e8Q
        {
            FigureMoving fm = new FigureMoving(move);
            if (!moves.CanMove(fm))
            {
                return this;
            }
            Board nextBoad = board.Move(fm);
            Chess nextChess = new Chess(nextBoad);
            return nextChess;
        }

        public char GetFigureAt(int x, int y)
        {
            Square square = new Square(x, y);
            EFigure fig = board.GetFigureAt(square);
            return fig == EFigure.None ? '.' : (char)fig;
        }
    }
}
