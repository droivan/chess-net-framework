﻿namespace Chess
{
    /// <summary>
    /// Цвет
    /// </summary>
    public enum EColor
    {
        /// <summary>
        /// Нет цвета
        /// </summary>
        None,

        /// <summary>
        /// Белый
        /// </summary>
        White,

        /// <summary>
        /// Чёрный
        /// </summary>
        Black
    }

    /// <summary>
    /// Extension color class
    /// </summary>
    static class ColorMethods
    {
        /// <summary>
        /// Flip color
        /// </summary>
        /// <param name="color">Source color</param>
        /// <returns>New color</returns>
        public static EColor FlipColor(this EColor color)
        {
            if (color == EColor.White)
            {
                return EColor.Black;
            }

            if (color == EColor.Black)
            {
                return EColor.White; 
            }

            return color;
        }
    }
}